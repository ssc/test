
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite lock)

(5am:test lock-name
  (5am:is (null (ssc:lock-name (ssc:make-lock))))
  (5am:is (string= "FOO" (ssc:lock-name (ssc:make-lock :name "FOO")))))

(5am:test lock-owner
  (flet ((f (l)
           (5am:is (null (ssc:lock-owner l)))
           (ssc:acquire-lock l)
           (5am:is (eq (ssc:current-thread) (ssc:lock-owner l)))
           (ssc:release-lock l)
           (5am:is (null (ssc:lock-owner l)))
           (ssc:with-lock (l)
             (5am:is (eq (ssc:current-thread) (ssc:lock-owner l))))
           (5am:is (null (ssc:lock-owner l)))))
    (f (ssc:make-lock :kind :errorcheck))
    (f (ssc:make-lock :kind :recursive))
    (f (ssc:make-lock :kind :no-errorcheck))))

(5am:test lockp
  (5am:is (ssc:lockp (ssc:make-lock :kind :errorcheck)))
  (5am:is (ssc:lockp (ssc:make-lock :kind :recursive)))
  (5am:is (ssc:lockp (ssc:make-lock :kind :no-errorcheck)))
  (5am:is (not (ssc:lockp t)))
  (5am:is (not (ssc:lockp nil))))

(macrolet ((m (test-name lock-kind)
             `(5am:test ,test-name
                (let ((l (ssc:make-lock :kind ,lock-kind)))
                  (ssc:with-lock (l) nil)
                  (5am:pass))
                (let ((l (ssc:make-lock :kind ,lock-kind)))
                  (ssc:acquire-lock l)
                  (ssc:release-lock l)
                 (5am:pass))
                (let ((l (ssc:make-lock :kind ,lock-kind)))
                  (ssc:acquire-lock l)
                  (ssc:release-lock l)
                  (ssc:acquire-lock l)
                   (ssc:release-lock l)
                  (5am:pass))
                (let ((l (ssc:make-lock :kind ,lock-kind)))
                  (ssc:with-lock (l) nil)
                  (ssc:with-lock (l) nil)
                  (5am:pass))
                 (let ((l (ssc:make-lock :kind ,lock-kind)))
                  (ssc:with-lock (l) nil)
                  (ssc:acquire-lock l)
                  (ssc:release-lock l)
                  (5am:pass))
                 (let ((l (ssc:make-lock :kind ,lock-kind)))
                  (ssc:acquire-lock l)
                  (ssc:release-lock l)
                  (ssc:with-lock (l) nil)
                  (5am:pass)))))
  (m lock.basic-acquire/release_errorcheck :errorcheck)
  (m lock.basic-acquire/release_recursive :recursive)
  (m lock.basic-acquire/release_no-errorcheck :no-errorcheck))

(5am:test lock.basic-recursive-acquire/release
  (let ((l (ssc:make-lock :kind :recursive)))
    (ssc:with-lock (l)
      (ssc:with-lock (l)
        (5am:pass))))
  (let ((l (ssc:make-lock :kind :recursive)))
    (ssc:with-lock (l)
      (ssc:acquire-lock l)
      (5am:pass)
      (ssc:release-lock l)))
  (let ((l (ssc:make-lock :kind :recursive)))
    (ssc:with-lock (l)
      (ssc:acquire-lock l)
      (5am:pass))
    (ssc:release-lock l))
  (let ((l (ssc:make-lock :kind :recursive)))
    (ssc:acquire-lock l)
    (ssc:with-lock (l)
      (5am:pass)
      (ssc:release-lock l)))
  (let ((l (ssc:make-lock :kind :recursive)))
    (ssc:acquire-lock l)
    (ssc:with-lock (l)
      (5am:pass))
    (ssc:release-lock l))
  (let ((l (ssc:make-lock :kind :recursive)))
    (ssc:acquire-lock l)
    (ssc:acquire-lock l)
    (5am:pass)
    (ssc:release-lock l)
    (ssc:release-lock l)))

(5am:test lock.condition-signaling_errorcheck
  (let ((l (ssc:make-lock :kind :errorcheck))
        (b (ssc:make-barrier 2)))
    (flet ((th ()
             (assert (signals-p ssc:lock-use-error (ssc:release-lock l)))
             (assert (not (ssc:try-acquire-lock l)))
             (ssc:pass-barrier b)
             (ssc:pass-barrier b)
             (assert (signals-p ssc:lock-use-error (ssc:release-lock l)))
             (assert (ssc:try-acquire-lock l))
             (ssc:release-lock l)
             (ssc:pass-barrier b)))
      (5am:signals ssc:lock-use-error (ssc:release-lock l))
      (ssc:acquire-lock l)
      (5am:signals ssc:lock-deadlock-error (ssc:acquire-lock l))
      (ssc:make-thread #'th)
      (ssc:pass-barrier b)
      (ssc:release-lock l)
      (5am:signals ssc:lock-use-error (ssc:release-lock l))
      (ssc:pass-barrier b)
      (ssc:pass-barrier b))))

(5am:test lock.condition-signaling_recursive
  (let ((l (ssc:make-lock :kind :recursive))
        (b (ssc:make-barrier 2)))
    (flet ((f ()
             (assert (signals-p ssc:lock-use-error (ssc:release-lock l)))
             (assert (not (ssc:try-acquire-lock l)))
             (ssc:pass-barrier b)
             (ssc:pass-barrier b)
             (assert (signals-p ssc:lock-use-error (ssc:release-lock l)))
             (assert (ssc:try-acquire-lock l))
             (ssc:release-lock l)
             (ssc:pass-barrier b)))
      (ssc:acquire-lock l)
      (ssc:acquire-lock l)
      (5am:is-true (ssc:try-acquire-lock l))
      (ssc:release-lock l)
      (ssc:release-lock l)
      (ssc:make-thread #'f)
      (ssc:pass-barrier b)
      (ssc:release-lock l)
      (5am:signals ssc:lock-use-error (ssc:release-lock l))
      (ssc:pass-barrier b)
      (ssc:pass-barrier b))))

(5am:test lock.contend-0
  (let* ((rounds 1000)
         (num-threads 100)
         (l (ssc:make-lock))
         (b (ssc:make-barrier (1+ num-threads))))
    (flet ((f ()
             (dotimes (i rounds)
               (ssc:acquire-lock l)
               (ssc:release-lock l)
               (sleep 0.00001))
             (ssc:pass-barrier b)))
      (ssc:acquire-lock l)
      (dotimes (i num-threads)
        (ssc:make-thread #'f))
      (ssc:release-lock l)
      (ssc:pass-barrier b)
      (5am:pass))))


(5am:test lock.contend-1
  (let* ((num-threads 5)
         (counter num-threads)
         (l (ssc:make-lock))
         (b (ssc:make-barrier (1+ num-threads))))
    (flet ((f ()
             (ssc:with-lock (l)
               (let ((c counter))
                 (waste-processor-time)
                 (setf counter (1- c))))
             (ssc:pass-barrier b)))
      (dotimes (i num-threads)
        (ssc:make-thread #'f))
      (5am:is-false (times-out-p (ssc:pass-barrier/timeout b 2)))
      (5am:is (zerop counter)))))

(5am:test lock.contend-2
  (let* ((rounds 2)
         (num-threads 10)
         shared-var
         (l (ssc:make-lock))
         (b (ssc:make-barrier (1+ num-threads))))
    (flet ((f ()
               (dotimes (i rounds)
                 (ssc:with-lock (l)
                   (let ((c (random 100)))
                     (setf shared-var c)
                     (waste-processor-time)
                     (assert (= shared-var c))))
                 (ssc:pass-barrier b))))
      (dotimes (i num-threads)
        (ssc:make-thread #'f))
      (5am:is-false (times-out-p (ssc:pass-barrier/timeout b 2))))))

