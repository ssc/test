
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite semaphore)

(5am:test semaphore-name
  (5am:is (null (ssc:semaphore-name (ssc:make-semaphore 0))))
  (let ((s (ssc:make-semaphore 0 :name "FOO")))
    (5am:is (string= "FOO" (ssc:semaphore-name s)))))

(5am:test semaphorep
  (5am:is-true (ssc:semaphorep (ssc:make-semaphore 0)))
  (5am:is-false (ssc:semaphorep t))
  (5am:is-false (ssc:semaphorep nil)))

(5am:test semaphore.down-1
  (let ((s (ssc:make-semaphore 2)))
    (5am:is (= 1 (ssc:down-semaphore s)))
    (5am:is (= 0 (ssc:down-semaphore s)))))

(5am:test semaphore.down-2
  (let* ((num-threads 5)
         (s (ssc:make-semaphore num-threads))
         (b (ssc:make-barrier (1+ num-threads))))
    (dotimes (i num-threads)
      (ssc:make-thread #'(lambda ()
                           (ssc:down-semaphore s)
                           (ssc:pass-barrier b))))
    (ssc:pass-barrier b)
    (5am:is (= 1 (ssc:up-semaphore s)))))

(5am:test semaphore.up
  (let ((s (ssc:make-semaphore -2)))
    (5am:is (= -1 (ssc:up-semaphore s)))
    (5am:is (= 0 (ssc:up-semaphore s)))
    (5am:is (= 1 (ssc:up-semaphore s)))
    (5am:is (= 2 (ssc:up-semaphore s)))))

(5am:test semaphore.down/up-1
  (let ((s (ssc:make-semaphore 1)))
    (5am:is (= 0 (ssc:down-semaphore s)))
    (5am:is (= 1 (ssc:up-semaphore s)))))

(5am:test semaphore.down/up-2
  (let ((s (ssc:make-semaphore 0))
        (b (ssc:make-barrier 2)))
    (ssc:make-thread #'(lambda ()
                         (ssc:pass-barrier b)
                         (assert (= 0 (ssc:down-semaphore s)))
                         (ssc:pass-barrier b)))
    (ssc:up-semaphore s)
    (ssc:pass-barrier b)
    (ssc:pass-barrier b)
    (5am:is (= 1 (ssc:up-semaphore s)))))

(5am:test semaphore.down/up-3
  (let ((s (ssc:make-semaphore 0))
        (b (ssc:make-barrier 2)))
    (ssc:make-thread #'(lambda ()
                         (ssc:pass-barrier b)
                         (assert (= 0 (ssc:down-semaphore s)))
                         (ssc:pass-barrier b)))
    (ssc:pass-barrier b)
    (sleep 0.3)            ; wait for the other thread to perform down
    (5am:is (= 1 (ssc:up-semaphore s)))
    (ssc:pass-barrier b)
    (5am:is (= 1 (ssc:up-semaphore s)))))

(5am:test semaphore.down/up-4
  (let* ((num-threads 10)
         (s (ssc:make-semaphore 0))
         (b (ssc:make-barrier (1+ num-threads))))
    (dotimes (i num-threads)
      (ssc:make-thread #'(lambda ()
                           (ssc:pass-barrier b)
                           (assert (= 0 (ssc:down-semaphore s)))
                           (ssc:pass-barrier b))))
    (ssc:pass-barrier b)
    (dotimes (i num-threads)
      (sleep 0.05)            ; wait for other threads to perform down
      (5am:is (= 1 (ssc:up-semaphore s))))
    (ssc:pass-barrier b)
    (5am:is (= 1 (ssc:up-semaphore s)))))

(5am:test semaphore.try-down
  (let ((s (ssc:make-semaphore 1)))
    (5am:is (zerop (ssc:try-down-semaphore s)))
    (5am:is (null (ssc:try-down-semaphore s)))))

