
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite barrier)

(5am:test barrier-name
  (5am:is (null (ssc:barrier-name (ssc:make-barrier 1))))
  (5am:is (string= "FOO" (ssc:barrier-name (ssc:make-barrier 1 :name "FOO")))))

(5am:test barrierp
  (5am:is (ssc:barrierp (ssc:make-barrier 1)))
  (5am:is (not (ssc:barrierp t)))
  (5am:is (not (ssc:barrierp nil))))

(5am:test basic-pass-barrier-1
  (5am:is-true (ssc:pass-barrier (ssc:make-barrier 1))))

(5am:test basic-pass-barrier-2
  (let (result)
    (with-barriers ((b1 2) (b2 2))
      (ssc:make-thread #'(lambda ()
                           (setf result (ssc:pass-barrier b1))
                           (ssc:pass-barrier b2)))
      (let ((r (ssc:pass-barrier b1)))
        (ssc:pass-barrier b2)
        (5am:is (member result '(t nil)))
        (5am:is (member r '(t nil)))
        (5am:is (if result (not r) r))))))

(5am:test basic-pass-barrier-3
  (let* ((num-threads 10)
         (b (ssc:make-barrier (1+ num-threads))))
    (dotimes (i num-threads)
      (ssc:make-thread #'(lambda ()
                            (ssc:pass-barrier b))))
    (ssc:pass-barrier b)
    (5am:pass)))

(5am:test basic-pass-barrier-reuse-1
  (let (result
        (b (ssc:make-barrier 2)))
    (ssc:make-thread #'(lambda ()
                          (setf result (ssc:pass-barrier b))
                          (ssc:pass-barrier b)))
    (let ((r (ssc:pass-barrier b)))
      (ssc:pass-barrier b)
      (5am:is (member result '(t nil)))
      (5am:is (member r '(t nil)))
      (5am:is (if result (not r) r)))))

(5am:test pass-barrier-1
  (let* ((num-threads 20)
         (rounds 20))
    (with-barriers ((b (1+ num-threads)) (b1 num-threads) (b2 num-threads))
      (flet ((th ()
               (dotimes (i rounds)
                 (when (ssc:pass-barrier b1)
                   (setf b1 (ssc:make-barrier num-threads)))
                 (when (ssc:pass-barrier b2)
                   (setf b2 (ssc:make-barrier num-threads))))
               (ssc:pass-barrier b)))
        (dotimes (i num-threads)
          (ssc:make-thread #'th))
        (ssc:pass-barrier b)
        (5am:pass)))))

(5am:test pass-barrier-2
  (let ((num-threads 2)
         (rounds 20))
    (with-barriers ((b (1+ num-threads)) (b1 num-threads) (b2 num-threads))
      (flet ((th ()
               (dotimes (i rounds)
                 (ssc:pass-barrier b1)
                 (ssc:pass-barrier b2))
               (ssc:pass-barrier b)))
        (dotimes (i num-threads)
          (ssc:make-thread #'th))
        (ssc:pass-barrier b)
        (5am:pass)))))

(5am:test pass-barrier-3
  (let* ((rounds 20)
         (num-threads 20)
         (l (ssc:make-lock))
         (b (ssc:make-barrier (1+ num-threads)))
         (barriers (make-array num-threads))
         (counters (make-array num-threads))
         (serial (make-array num-threads)))
    (flet ((th (th-num)
             (dotimes (i rounds)
               (when (zerop th-num)
                 (dotimes (i num-threads)
                   (setf (svref counters i) 0)
                   (setf (svref serial i) 0)))
               (ssc:pass-barrier (svref barriers (1- num-threads)))
               (do ((j th-num (1+ j)))
                   ((>= j num-threads))
                 ;; Increment the counter for this round.
                 (ssc:with-lock (l)
                   (incf (svref counters j)))
                 (let ((x (ssc:pass-barrier (svref barriers j))))
                   (when (zerop th-num)
                     (assert (= (1+ j) (svref counters j))))
                   (when x
                     (ssc:with-lock (l)
                       (incf (svref serial j)))))
                 (ssc:pass-barrier (svref barriers j))
                 (assert (or (not (zerop th-num))
                             (= 1 (svref serial j))))))
             (ssc:pass-barrier b)))
      (dotimes (i num-threads)
        (setf (svref barriers i) (ssc:make-barrier (1+ i))))
      (dotimes (i num-threads)
        (let ((i i))
          (ssc:make-thread #'(lambda () (th i)))))
      (ssc:pass-barrier b)
      (5am:pass))))

