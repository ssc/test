
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite condvar)

(5am:test condvar-lock
  (5am:is (ssc:lockp (ssc:condvar-lock (ssc:make-condvar))))
  (let ((l (ssc:make-lock)))
    (5am:is (eq l (ssc:condvar-lock (ssc:make-condvar :lock l))))))

(5am:test condvar-name
  (5am:is (null (ssc:condvar-name (ssc:make-condvar))))
  (5am:is (string= "FOO" (ssc:condvar-name (ssc:make-condvar :name "FOO")))))

(5am:test condvarp
  (5am:is (ssc:condvarp (ssc:make-condvar)))
  (5am:is (not (ssc:condvarp t)))
  (5am:is (not (ssc:condvarp nil))))

(5am:test condvar.wait/signal-1
  "SIGNAL-CONDVAR after WAIT-CONDVAR does not unblock"
  (with-barriers ((b 2))
    (with-lock-and-condvars (l cv)
      (flet ((f ()
               (ssc:with-lock (l)
                 (ssc:signal-condvar cv))
               (ssc:pass-barrier b)))
        (ssc:with-lock (l)
          (ssc:make-thread #'f)
          (ssc:wait-condvar cv))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b 1)))))))

(5am:test condvar.wait/signal-all-1
  "SIGNAL-CONDVAR :ALLP T after WAIT-CONDVAR does not unblock"
  (with-barriers ((b 2))
    (with-lock-and-condvars (l cv)
      (flet ((f ()
               (ssc:with-lock (l)
                 (ssc:signal-condvar cv :allp t))
               (ssc:pass-barrier b)))
        (ssc:with-lock (l)
          (ssc:make-thread #'f)
          (ssc:wait-condvar cv))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b 1)))))))

;; Can fail because of spurious wakeups
(5am:test condvar.wait/signal-2
  "SIGNAL-CONDVAR before WAIT-CONDVAR unblocks"
  (with-barriers ((b 2))
    (with-lock-and-condvars (l cv)
      (flet ((f ()
               (ssc:with-lock (l)
                 (ssc:wait-condvar cv))
               (ssc:pass-barrier b)))
        (ssc:with-lock (l)
          (ssc:make-thread #'f)
          (ssc:signal-condvar cv))
        (5am:is-true (times-out-p (ssc:pass-barrier/timeout b 1)))))))

;; Can fail because of spurious wakeups
(5am:test condvar.wait/signal-all-2
  "SIGNAL-CONDVAR :ALLP T before WAIT-CONDVAR unblocks"
  (with-barriers ((b 2))
    (with-lock-and-condvars (l cv)
      (flet ((f ()
               (ssc:with-lock (l)
                 (ssc:wait-condvar cv))
               (ssc:pass-barrier b)))
        (ssc:with-lock (l)
          (ssc:make-thread #'f)
          (ssc:signal-condvar cv :allp t))
        (5am:is-true (times-out-p (ssc:pass-barrier/timeout b 1)))))))

(5am:test condvar.wait/signal-all-3
  "SIGNAL-CONDVAR :ALLP T does not work"
  (let* ((num-threads 10)
         (threads-iro (make-threads-iro num-threads)))
    (with-barriers ((b (1+ num-threads)))
      (with-lock-and-condvars (l cv)
        (flet ((f ()
                 (threads-iro.add-current-thread threads-iro)
                 (ssc:with-lock (l)
                   (loop until (threads-iro.my-turn-p threads-iro)
                         do (ssc:wait-condvar cv))
                   (ssc:signal-condvar cv :allp t))
                 (ssc:pass-barrier b)))
          (ssc:with-lock (l)
            (dotimes (i num-threads)
              (ssc:make-thread #'f)))
          (5am:is-false (times-out-p (ssc:pass-barrier/timeout b 1))))))))

(5am:test condvar-4
  (with-barriers ((b1 2) (b2 2))
    (with-lock-and-condvars (l cv)
      (flet ((f ()
               (ssc:with-lock (l)
                 (ssc:pass-barrier b1)
                 (ssc:wait-condvar cv)
                 (ssc:pass-barrier b1)
                 (ssc:signal-condvar cv)
                 (ssc:pass-barrier b1)))
             (g ()
               (ssc:with-lock (l)
                 (ssc:pass-barrier b2)
                 (ssc:signal-condvar cv)
                 (ssc:pass-barrier b2)
                 (ssc:wait-condvar cv)
                 (ssc:pass-barrier b2))))
        (ssc:make-thread #'f)
        (ssc:make-thread #'g)
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b1 1)))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b2 1)))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b2 1)))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b1 1)))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b1 1)))
        (5am:is-false (times-out-p (ssc:pass-barrier/timeout b2 1)))))))

(5am:test condvar-5
  (let ((num-threads 10))
    (with-barriers ((b 2))
      (with-lock-and-condvars (l cv)
        (flet ((f ()
                 (ssc:acquire-lock l)
                 (ssc:wait-condvar cv)
                 (ssc:pass-barrier b)))
          (dotimes (i num-threads)
            (ssc:make-thread #'f))
          (5am:is-true (times-out-p (ssc:pass-barrier/timeout b 2))))))))
