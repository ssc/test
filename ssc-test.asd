;; -*- mode: lisp -*-

(asdf:defsystem :ssc-test
  :description "Common Lisp SSC test suite"
  :version "1"
  :author "Marco Monteiro <masm@acm.org>"
  :licence "GPL"
  :depends-on ("fiveam" "ssc")
  :components
  ((:file "packages")
   (:file "suite" :depends-on ("packages"))
   (:file "thread" :depends-on ("suite"))
   (:file "lock" :depends-on ("suite"))
   (:file "rwlock" :depends-on ("suite"))
   (:file "condvar" :depends-on ("suite"))
   (:file "semaphore" :depends-on ("suite"))
   (:file "barrier" :depends-on ("suite"))
   (:file "mailbox" :depends-on ("suite"))))
