
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:def-suite ssc :description "Shared State Concurrency test suite.")

(5am:def-suite thread :in ssc)
(5am:def-suite lock :in ssc)
(5am:def-suite rwlock :in ssc)
(5am:def-suite condvar :in ssc)
(5am:def-suite semaphore :in ssc)
(5am:def-suite barrier :in ssc)
(5am:def-suite mailbox :in ssc)

(defun kill-if-alive (thread)
  (if (member thread (ssc:alive-threads))
      (ssc:kill-thread thread)))

(defun kill-alive-threads (&rest threads)
  (dolist (thread threads)
    (kill-if-alive thread)))

(defmacro stop-thread-until (predicate &key time)
  (if (eql time nil)
      `(do () (,predicate)
        (ssc:thread-yield))
      (let ((sym (gensym)))
        `(let ((,sym (1+ (* 10 ,time))))
          (do () ((or ,predicate (= 0 (decf ,sym))))
            #- (and) (ssc:thread-yield) ; for non-preemptive scheduling
            (sleep 0.1))))))

(defun wait-for-threads (&rest threads)
  (loop while (some #'ssc:thread-alive-p threads)
        do (progn
             (sleep 0.1))))

(defun waste-processor-time ()
  (labels ((fib (n)
           (if (< n 2)
               1
               (+ (fib (- n 1))
                  (fib (- n 2))))))
    (fib (+ 14 (random 14)))))

(defmacro define-batch-tests (name functions)
  `(defun ,name ()
     (dolist (f ,functions)
       (print f)
       (funcall f))))

(defun ensure-failure (v)
  (declare (ignore v))
  (error "Control should not have reached here."))

(defmacro signals-p (condition form)
  `(handler-case (progn ,form nil)
     (,condition () t)))

(defun times-out-p (pred)
  (not pred))

;; TODO: comment
(defstruct (threads-iro (:constructor make-threads-iro (num-threads)))
  (index/add (random num-threads))
  (index/run 0)
  (num-threads num-threads)
  (num-threads-missing num-threads)
  (vector (make-array num-threads :initial-element nil))
  (mutex (ssc:make-lock)))

(defun threads-iro.my-turn-p (threads-iro)
  (ssc:with-lock ((threads-iro-mutex threads-iro))
    (let ((index (threads-iro-index/run threads-iro))
          (vector (threads-iro-vector threads-iro)))
      (assert (< index (threads-iro-num-threads threads-iro)))
      (if (eq (ssc:current-thread) (svref vector index))
          (incf (threads-iro-index/run threads-iro))
          nil)))) 

(flet (;; Search VECTOR for index of the first NIL element to the left
       ;; of INDEX
       (search-left (vector index size)
         (declare (ignore size))
         (do ((i index (1- i)))
             ((or (< i 0) (null (svref vector i))) (and (>= i 0) i))))
       ;; Search VECTOR for index of the first NIL element to the right
       ;; of INDEX
       (search-right (vector index size)
         (do ((i index (1+ i)))
             ((or (= i size) (null (svref vector i))) (and (< i size) i)))))

  ;; Add the SSC:CURRENT-THREAD to the THREADS-IRO and set index for
  ;; the next addition
  (defun threads-iro.add-current-thread (threads-iro)
    (ssc:with-lock ((threads-iro-mutex threads-iro))
      (let ((index (threads-iro-index/add threads-iro))
            (num-threads (threads-iro-num-threads threads-iro))
            (vector (threads-iro-vector threads-iro)))
        (assert (> (threads-iro-num-threads-missing threads-iro) 0))
        (assert (< index num-threads))
        (assert (null (svref vector index)))
        (setf (svref vector index) (ssc:current-thread))
        ;; Prepare index for next addition.
        (unless (zerop (decf (threads-iro-num-threads-missing threads-iro)))
          (let ((next-index (random num-threads)))
            (unless (null (svref vector next-index))
              (let (first-func second-func)
                (if (zerop (random 2))
                    (setf first-func #'search-left
                          second-func #'search-right)
                    (setf first-func #'search-right
                          second-func #'search-left))
                (let ((i (funcall first-func vector next-index num-threads)))
                  (if i
                      (setf next-index i)
                      (setf next-index (funcall second-func vector
                                                next-index num-threads))))))
            (assert (not (null next-index)))
            (assert (null (svref vector next-index)))
            (setf (threads-iro-index/add threads-iro) next-index)))))))

(defmacro with-lock-and-condvars ((lock &rest condvars) &body body)
  `(let ((,lock (ssc:make-lock)))
     ,(let ((bindings (mapcar #'(lambda (cv)
                                  `(,cv (ssc:make-condvar :lock ,lock)))
                             condvars)))
        `(let (,@bindings)
           ,@body))))

(defmacro with-barriers (barriers-and-numbers &body body)
  (let ((bindings (mapcar #'(lambda (b)
                              `(,(car b) (ssc:make-barrier ,(cadr b))))
                          barriers-and-numbers)))
    `(let (,@bindings)
      ,@body)))
