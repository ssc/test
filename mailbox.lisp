
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite mailbox)

(5am:test mailbox.basic-1
  "tests THREAD-SEND, THREAD-RECEIVE, and THREAD-TRY-RECEIVE"
  (ssc:thread-send (ssc:current-thread) 1)
  (5am:is (= 1 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 2)
  (ssc:thread-send (ssc:current-thread) 3)
  (5am:is (= 2 (ssc:thread-receive)))
  (5am:is (= 3 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 4)
  (5am:is (= 4 (ssc:thread-receive)))
  (ssc:thread-send (ssc:current-thread) 5)
  (5am:is (= 5 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 6)
  (ssc:thread-send (ssc:current-thread) 7)
  (ssc:thread-send (ssc:current-thread) 8)
  (5am:is (= 6 (ssc:thread-receive)))
  (5am:is (= 7 (ssc:thread-receive)))
  (5am:is (= 8 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 9)
  (ssc:thread-send (ssc:current-thread) 10)
  (5am:is (= 9 (ssc:thread-receive)))
  (ssc:thread-send (ssc:current-thread) 11)
  (5am:is (= 10 (ssc:thread-receive)))
  (5am:is (= 11 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 12)
  (ssc:thread-send (ssc:current-thread) 13)
  (5am:is (= 12 (ssc:thread-receive)))
  (5am:is (= 13 (ssc:thread-receive)))
  (ssc:thread-send (ssc:current-thread) 14)
  (5am:is (= 14 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 15)
  (5am:is (= 15 (ssc:thread-receive)))
  (ssc:thread-send (ssc:current-thread) 16)
  (ssc:thread-send (ssc:current-thread) 17)
  (5am:is (= 16 (ssc:thread-receive)))
  (5am:is (= 17 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive))))
  (ssc:thread-send (ssc:current-thread) 18)
  (5am:is (= 18 (ssc:thread-receive)))
  (ssc:thread-send (ssc:current-thread) 19)
  (5am:is (= 19 (ssc:thread-receive)))
  (ssc:thread-send (ssc:current-thread) 20)
  (5am:is (= 20 (ssc:thread-receive)))
  (5am:is-true (null (nth-value 1 (ssc:thread-try-receive)))))

(5am:test mailbox-1
  (let ((th (ssc:current-thread))
        (std *standard-output*))
    (flet ((f ()
             (let ((*standard-output* std))
               (assert (= 1 (ssc:thread-receive)))
               (ssc:thread-send th 2)
               (dotimes (i 10)
                 (ssc:thread-send th i)
                 (assert (= i (ssc:thread-receive)))
                 (waste-processor-time)))))
      (let ((th (ssc:make-thread #'f)))
        (ssc:thread-send th 1)
        (5am:is (= 2 (ssc:thread-receive)))
        (dotimes (i 10)
          (ssc:thread-send th i)
          (5am:is (= i (ssc:thread-receive)))
          (waste-processor-time))))))


(5am:test mailbox-2
  (let ((th (ssc:current-thread)))
    (flet ((f ()
             (dotimes (i 5)
               (assert (= i (ssc:thread-receive)))
               (ssc:thread-send th i))))
      (let ((ths (make-array 10)))
        (dotimes (i 10)
          (setf (svref ths i) (ssc:make-thread #'f)))
        (dotimes (i 5)
          (dotimes (j 10)
            (ssc:thread-send (svref ths j) i))
          (dotimes (j 10)
            (5am:is (= i (ssc:thread-receive)))))))))

(5am:test mailbox-3
  (let ((th (ssc:current-thread)))
    (flet ((f ()
             (dotimes (i 10)
               (ssc:thread-send th i)
               (waste-processor-time))))
      (let ((ths (make-array 10))
            (array (make-array 10 :initial-element 0)))
        (dotimes (i 10)
          (setf (svref ths i) (ssc:make-thread #'f)))
        (dotimes (i 100)
          (incf (svref array (ssc:thread-receive))))
        (dotimes (i 10)
          (5am:is (= 10 (svref array i))))))))
