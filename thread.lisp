
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite thread)

(5am:test alive-threads
  (5am:is (member (ssc:current-thread) (ssc:alive-threads)))
  (let* ((b (ssc:make-barrier 2)))
    (ssc:make-thread #'(lambda ()
                          (ssc:pass-barrier b)
                          (ssc:pass-barrier b)))
    (ssc:pass-barrier b)
    (5am:is (member (ssc:current-thread) (ssc:alive-threads)))
    (ssc:pass-barrier b)))

(5am:test current-thread
  (5am:is (not (null (ssc:current-thread))))
  (5am:is (eq (ssc:current-thread) (ssc:current-thread)))
  (let* (c
         (b (ssc:make-barrier 2))
         (th (ssc:make-thread #'(lambda ()
                                   (setf c (ssc:current-thread))
                                   (ssc:pass-barrier b)))))
    (ssc:pass-barrier b)
    (5am:is (eq c th))
    (5am:is (not (eq (ssc:current-thread) c)))))

(5am:test thread-alive-p
  (5am:is (ssc:thread-alive-p (ssc:current-thread)))
  (let* ((b (ssc:make-barrier 2))
         (th (ssc:make-thread #'(lambda ()
                                  (ssc:pass-barrier b)
                                  (ssc:pass-barrier b)))))
    (ssc:pass-barrier b)
    (5am:is (ssc:thread-alive-p th))
    (ssc:pass-barrier b)))

(5am:test thread-name
  (5am:is (null (ssc:thread-name (ssc:make-thread #'(lambda () nil)))))
  (let ((th (ssc:make-thread #'(lambda () nil) :name "FOO")))
    (5am:is (eq "FOO" (ssc:thread-name th)))))

(5am:test threadp
  (5am:is (ssc:threadp (ssc:current-thread)))
  (5am:is (ssc:threadp (ssc:make-thread #'(lambda () nil))))
  (5am:is (not (ssc:threadp t)))
  (5am:is (not (ssc:threadp nil))))
