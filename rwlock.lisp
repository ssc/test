
;; This file is part of the Common Lisp SSC test suite
;; Copyright (C) 2006  Marco Monteiro

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

(cl:in-package "SSC-TEST")

(5am:in-suite rwlock)

(5am:test rwlock-name
  (5am:is (null (ssc:rwlock-name (ssc:make-rwlock))))
  (5am:is (string= "FOO" (ssc:rwlock-name (ssc:make-rwlock :name "FOO")))))

(5am:test rwlockp
  (5am:is-true (ssc:rwlockp (ssc:make-rwlock :bias :fair)))
  (5am:is-true (ssc:rwlockp (ssc:make-rwlock :bias :read)))
  (5am:is-true (ssc:rwlockp (ssc:make-rwlock :bias :write)))
  (5am:is-false (ssc:rwlockp t))
  (5am:is-false (ssc:rwlockp nil)))

(5am:test rwlock.acquire/release-1
  (flet ((f (bias)
          (let ((l (ssc:make-rwlock :bias bias)))
            (5am:finishes (ssc:acquire-rwlock l :read))
            (5am:finishes (ssc:release-rwlock l)))
          (let ((l (ssc:make-rwlock :bias bias)))
            (5am:finishes (ssc:acquire-rwlock l :write))
            (5am:finishes (ssc:release-rwlock l)))))
    (dolist (b '(:fair :read :write))
      (f b))))

(5am:test rwlock.with-1
  (flet ((f (bias)
          (let ((l (ssc:make-rwlock :bias bias)))
            (5am:finishes (ssc:with-rwlock (l :read) nil)))
          (let ((l (ssc:make-rwlock :bias bias)))
            (5am:finishes (ssc:with-rwlock (l :write) nil)))))
    (dolist (b '(:fair :read :write))
      (f b))))

(5am:test rwlock.acquire/release-2
  (flet ((f (bias)
          (let ((l (ssc:make-rwlock :bias bias)))
            (5am:finishes (ssc:acquire-rwlock l :read))
            (5am:finishes (ssc:release-rwlock l))
            (5am:finishes (ssc:acquire-rwlock l :write))
            (5am:finishes (ssc:release-rwlock l))
            (5am:finishes (ssc:acquire-rwlock l :read))
            (5am:finishes (ssc:release-rwlock l))
            (5am:finishes (ssc:acquire-rwlock l :write))
            (5am:finishes (ssc:release-rwlock l)))))
    (dolist (b '(:fair :read :write))
      (f b))))

(5am:test rwlock.with-2
  (flet ((f (bias)
          (let ((l (ssc:make-rwlock :bias bias)))
            (5am:finishes (ssc:with-rwlock (l :read) nil))
            (5am:finishes (ssc:with-rwlock (l :write) nil))
            (5am:finishes (ssc:with-rwlock (l :read) nil))
            (5am:finishes (ssc:with-rwlock (l :write) nil)))))
    (dolist (b '(:fair :read :write))
      (f b))))

(5am:test rwlock.acquire/release-3
  (flet ((f (bias)
           (let ((l (ssc:make-rwlock :bias bias))
                 (b (ssc:make-barrier 2)))
             (ssc:make-thread #'(lambda ()
                                  (ssc:acquire-rwlock l :read)
                                  (ssc:pass-barrier b)
                                  (ssc:release-rwlock l)
                                  (ssc:pass-barrier b)))
             (ssc:acquire-rwlock l :read)
             (5am:finishes (ssc:pass-barrier b))
             (ssc:release-rwlock l)
             (5am:finishes (ssc:pass-barrier b)))))
    (dolist (b '(:fair :read :write))
      (f b))))

(5am:test rwlock.with-3
  (flet ((f (bias)
           (let ((l (ssc:make-rwlock :bias bias))
                 (b (ssc:make-barrier 2)))
             (ssc:make-thread #'(lambda ()
                                  (ssc:with-rwlock (l :read)
                                    (ssc:pass-barrier b))
                                  (ssc:pass-barrier b)))
             (ssc:with-rwlock (l :read)
               (5am:finishes (ssc:pass-barrier b)))
             (5am:finishes (ssc:pass-barrier b)))))
    (dolist (b '(:fair :read :write))
      (f b))))

